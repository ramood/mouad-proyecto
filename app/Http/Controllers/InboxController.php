<?php

namespace App\Http\Controllers;
use App\Models\chat;
use Illuminate\Http\Request;
use Auth;
use App\Mail\Mails;
Use Mail;

class InboxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', Chat::class); 
        session()->forget('inbox');
        $user=Auth::user();
        $query=Chat::query();
        $query->orderBy('created_at', 'desc')->get();
        $messages=$query->paginate(8);
        return view('contact.inbox',["user"=>$user,'messages'=>$messages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
    }
    public function reply(Request $request,$id)
    {$user=Auth::user();
    $message=Chat::find($id);
       $reply=$request->reply;
       $details=[
        'title'=>'Replying to your comment',
        'reply'=>$reply,
        'message'=>$message->comment,

    ];
    Mail::to($message->email)->send(new Mails($details));
       return view('contact.show',["message"=>$message,"user"=>$user,"reply"=>$reply]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   $this->authorize('view', Chat::class); 
        $user=Auth::user();
        $message=Chat::find($id);
        $message->statut="read";
        $message->save();
        return view('contact.show',["message"=>$message,"user"=>$user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $this->authorize('delete', Chat::class); 
        $chat=Chat::find($id);
        $chat->delete();
        return back()->with('success','Conversation deleted');
    }
}
