<?php

namespace App\Http\Controllers;

use App\Models\category;
use App\Models\product;
use App\Models\chat;
use App\Models\order;
use Illuminate\Http\Request;
use Session;
use Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   //Importar los mensajes no leidos
        $query=Chat::query();
        $query->where('statut','like',"unread")->get();
        $inbox=session()->get('inbox');
        $inbox=$query->paginate(8);
        session()->put('inbox',$inbox); 
        //Fin mensajes
        $query=order::query();
        $query->where('status','like',"in progress")->get();
        $orders=session()->get('orders');
        $orders=$query->paginate(8);
        session()->put('orders',$orders);
       

        $user=Auth::user();
        $product=Product::find(2);
        $query=Product::query();
        // filros de productos
        if($request->search)
        {
            $query->where('name','like',"%$request->search%")->where('quantity','>=',"1")->orWhere('description','like',"%$request->search%")->orderBy('created_at', 'desc')->get();
        }// fin filtros

        //importar categorias
        $categories=Category::all();
        //fin categorias

        //importar todos los productos que existen en el almacen
        $query->where('quantity','>=',"1")->orderBy('created_at', 'desc')->get();;
        // fin productos 

        $products=$query->paginate(9);//paginacion
        return view('products.index',['products'=>$products,'categories'=>$categories,'user'=>$user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Product::class); //autorizar solo el admin 
        $categories=Category::all();
        return view('products.create',['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->authorize('create', Product::class);// solo el admin que tiene permiso de añadir productos
        //validacion de campos
        $rules=['name'=>'required|unique:products|max:50','description'=>'required|max:300','price'=>'required|numeric','quantity'=>'required|numeric',
        'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|','category_id'=>'required'];
        $request->validate($rules);
        //fin validacion

           $name = $request->file('image')->getClientOriginalName();//link de la imagen
           $save = new Product;
           $save->name =$request->name;
           $save->image = $name;
           $save->quantity=$request->quantity;
           $save->price=$request->price;
           $save->description=$request->description;
           $save->category_id=$request->category_id;
           $save->save();//guardar el nuevo producto

           return redirect('/shop');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {  
        $user=Auth::user();
        $product=Product::find($id);//buscar el producto seleccionado
        $categories=Category::all();//importar todas las categorias 
        $available=1;
        if($product->quantity>1){ // comprobar si el producto existe en el almacen
            $available=1;
        }
        return view('products.detail', ['product' => $product,'categories'=>$categories,'available'=>$available,'user'=>$user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(product $product,$id)
    {
        $this->authorize('update', Product::class); 
        $product=Product::find($id);
        $categories=Category::all();
        return view('products.edit', ['product' => $product,'categories'=>$categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update', Product::class);
        $product = Product::find($id);
        $rules=['name'=>'required|max:50','description'=>'required|max:150','price'=>'required|numeric','quantity'=>'required|numeric',
        'category_id'=>'required'];
        $request->validate($rules);
        if($request->hasFile('image')){
            $request->validate([
              'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);
            
            $product->image = $request->file('image')->getClientOriginalName();
        }
        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->quantity = $request->quantity;
        $product->category_id = $request->category_id;
        $product->save();
    
        return redirect('shop')
                        ->with('success','Post updated successfully');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\product  $product
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $this->authorize('delete', Product::class);
        $product=Product::find($id);
        $product->delete();
        return back()->with('success','product deleted');
    }
    public function sortbyCateg(Request $request,$name,$id)
    {   
        $user=Auth::user();
       $min=$request->min;
        $max=$request->max;
        $query=Product::query();
        if($min && $max)
        {
            if(($id==0))
            {
                $query->whereBetween('price',[$min,$max])->where('quantity','>=',"1");;
            }else{
                $query->whereBetween('price',[$min,$max])->where('category_id','like',"$id")->where('quantity','>=',"1");;
            } 
        }
        
        else{
            $query->where('category_id','like',"$id")->where('quantity','>=',"1");;
        }
       
        $categories=Category::all();
        $products=$query->paginate(9);
        return view('products.index', ['products' => $products,'categories'=>$categories,'name'=>$name,'id'=>$id,'user'=>$user]);
    }
 
}
