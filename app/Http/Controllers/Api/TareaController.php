<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Models\Tarea;
use PhpParser\Node\Stmt\Return_;
class TareaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    
    public function index()
    {
        
        $tareas = Tarea::all();
        // return $tareas;
        return response()->json([
            'status' => 200,
            'data' => $tareas
        ], 200);
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $user = \Auth::user();
        if ($user->can('create', Tarea::class)) {
        $rules = [
            'name' => 'required|max:255'
        ];        
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'errors' => $validator->errors()
            ], 422);                
        }
        
        $tarea = Tarea::create($request->all());
        return response()->json([
            'status' => 'ok',
            'data' => $tarea
        ], 201);
    }
    else{
        return response()->json([
            'status' => 403,
            'message' => 'No autorizado'
        ], 403);    
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tarea = Tarea::find($id);

        $this->check404($tarea);

        return response()->json([
            'status' => 'ok',
            'data' => $tarea
        ], 200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    public function patch(Request $request, $id)
    {
        $tarea = Tarea::find($id);     

        $this->check404($tarea);
        $user = \Auth::user();
        if ($user->can('update', Tarea::class)) {
        //modificar name        
        if ($request->name) {
            //si no pasa validación 422
            $rules = [
                'name' => 'required|max:255',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status' => 'error',
                    'errors' => $validator->errors()
                ], 422);                
            }
        }

        //modificar code        
        if ($request->code) {
            //si no pasa validación 422
            $rules = [
                'code' => 'required|max:255',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status' => 'error',
                    'errors' => $validator->errors()
                ], 422);                
            }
        }

        //todo ok 200
        $tarea->fill($request->all());
        $tarea->save();
        return response()->json([
            'status' => 'ok',
            'data' => $tarea
        ], 200);
    }
    else{
        return response()->json([
            'status' => 403,
            'message' => 'No autorizado'
        ], 403);    
    }
    }

    public function validarCampo($campo, $regla)
    {
        # code...
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = \Auth::user();
        if ($user->can('update', Tarea::class)) {
        if ($request->method() == 'PATCH') {
            return $this->patch($request, $id);
        }
        $tarea = Tarea::find($id);     
        $this->check404($tarea);

        //si no pasa validación 422
        $rules = [
            'name' => 'required|max:255'
        ];        
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'errors' => $validator->errors()
            ], 422);                
        }

        //todo ok 200
        $tarea->fill($request->all());
        $tarea->save();
        return response()->json([
            'status' => 'ok',
            'data' => $tarea
        ], 200);   
    }
    else{
        return response()->json([
            'status' => 403,
            'message' => 'No autorizado'
        ], 403);    
    }             
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = \Auth::user();
        if ($user->can('delete', Tarea::class)) {
        $tarea = Tarea::find($id);
        $this->check404($tarea);

        try {
            //status 204: No content
            $tarea->delete();
            return response()->json([
                'Sin contenido'
            ], 204);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'error',
                'message' => 'Borrado fallido. Conflicto',
                'error_message' => $th->getMessage()
            ], 409);
        }
    }
    else{
        return response()->json([
            'status' => 403,
            'message' => 'No autorizado'
        ], 403);    
    }             
    }

    //DRY. Don't Repeat Yourself
    public function check404($tarea)
    {
        if (!$tarea) {
            response()->json([
                'status' => 404,
                'message' => 'No se ha encontrado tarea con ese id'
            ], 404)->send();
            die();
        }
    }    
}

