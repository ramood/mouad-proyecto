<?php

namespace App\Policies;

use App\Models\User;
use App\Models\order;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;

    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        if($user->role_id==3)
        {return $user->isAdmin();  }
        if($user->role_id==2)
        {return $user->isDelivery();  }
         
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\order  $order
     * @return mixed
     */
    public function view(User $user, order $order)
    {
        if($user->role_id==3)
        {return $user->isAdmin();  }
        if($user->role_id==2)
        {return $user->isDelivery();  } 
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if($user->role_id==3)
        {return $user->isAdmin();  }
        if($user->role_id==2)
        {return $user->isDelivery();  } 
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\order  $order
     * @return mixed
     */
    public function update(User $user)
    {
        if($user->role_id==3)
        {return $user->isAdmin();  }
        if($user->role_id==2)
        {return $user->isDelivery();  }
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\order  $order
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->isAdmin();  
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\order  $order
     * @return mixed
     */
    public function restore(User $user)
    {
        return $user->isAdmin();  
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\order  $order
     * @return mixed
     */
    public function forceDelete(User $user)
    {
        return $user->isAdmin();  
    }
}
