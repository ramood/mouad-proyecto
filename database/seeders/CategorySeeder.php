<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;


class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'id' => '1',
            'name' => 'computers'
        ]);
        DB::table('categories')->insert([
            'id' => '2',
            'name' => 'storage'
        ]);
        DB::table('categories')->insert([
            'id' => '3',
            'name' => 'peripherals'
        ]);
        DB::table('categories')->insert([
            'id' => '4',
            'name' => 'Image & sound'
        ]);
        DB::table('categories')->insert([
            'id' => '5',
            'name' => 'Gaming'
        ]);
    }
}
