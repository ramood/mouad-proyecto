<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //con DB
       
        DB::table('users')->insert([
            'id' => '3',
            'name' => 'admin',
            'email' => 'admin@dws.es',
            'password' => bcrypt('admin'),
            'role_id' => 3
        ]);
        DB::table('users')->insert([
            'id' => '4',
            'name' => 'Salvador',
            'email' => 'repartidor1@gmail.com',
            'password' => bcrypt('repartidor'),
            'role_id' => 2
        ]);

        User::factory()
        ->count(100)
        ->create();  
    }
}
