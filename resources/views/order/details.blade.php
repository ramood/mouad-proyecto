@extends('layouts.app')
@section('content')
<main id="main" class="main-site">

		<div class="container">
        @if (\Session::has('success'))
    <div class="alert alert-success">
       
          {!! \Session::get('success') !!}
        
    </div>
@endif
@if (\Session::has('error'))
    <div class="alert alert-danger">
       
            {!! \Session::get('error') !!}
       
    </div>
@endif
			<div class="wrap-breadcrumb">
				<ul>
					<li class="item-link"><a href="#" class="link">Order</a></li>
					<li class="item-link"><span>detail</span></li>
				</ul>
			</div>
			<div class=" main-content-area">
            
				<div class="wrap-iten-in-cart">
					<h3 class="box-title">Order N'{{$order->id}}</h3>
					<ul class="products-cart">
                    <li class="pr-cart-item">
							<div class="product-image">
                            <p class="price" style="font-size:16px;"><b>image</b></p>
							</div>
							<div class="product-name">
                            <p class="price" style="font-size:16px;"><b>Product name</b></p>
							</div>
							<div class="price-field produtc-price"><p class="price">Price</p></div>
							<div class="quantity">
                            <p class="price" style="font-size:16px;"><b>Quantity</b></p>
							</div>
							<div class="price-field sub-total"><p class="price">Total</p></div>
							<div class="delete">
								<a href="#" class="btn btn-delete" title="">
									<span>Delete from your cart</span>
									
								</a>
							</div>
                        </li>
                        
                        @foreach($details as $detail)
						<li class="pr-cart-item">
							<div class="product-image">
								<figure><img src="{{ asset('assets/images/products/' . $detail->product->image) }}" /></figure>
							</div>
							<div class="product-name">
								<a class="link-to-product" href="#">{{$detail->product->name}}</a>
							</div>
							<div class="price-field produtc-price"><p class="price">{{$detail->product->price}}$</p></div>
							<div class="quantity">
                            <div style="font-size:16px;">
                                    <p><b> {{$detail->quantity}} </b>
                            </div>
							</div>
							<div class="price-field sub-total"><p class="price">{{$detail->product->price * $detail->quantity}} $</p></div>
							
                          
                        </li>
    @endforeach	
  				
					</ul>
				</div>

			
             


			</div><!--end main content area-->
		</div><!--end container-->

    </main>
   
    @endsection