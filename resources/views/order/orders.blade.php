@extends('layouts.app')

@section('content')

<div class="container" style="margin-top:70px;margin-bottom:50px">
    <div class="row justify-content-center">
        <div class="col-md-12">
    
    <h1>Orders</h1>
            @if(session()->has('status'))
            <p class="alert alert-success">{{session('status')}}</p>
            @endif
    <table class="table table-striped"> 
        @csrf
        @method('PUT')
    <tr>
    <th> Number</th>
    <th>City</th>
    <th>Address</th>
    <th>Phone</th>
    <th>Payment Method</th>
    <th>Status</th>
    <th>Order date</th>
    <th>Total price</th>
    
    
    
    
    </tr>
    @forelse($orders as $order)
    <tr>
        <td>{{$order->id}}</td>
        <td>{{$order->city}}</td>
        <td>{{$order->adress}}</td>
        <td>{{$order->phone}}</td>
        <td>{{$order->payed}}</td>
        @if($order->status=="in progress")
        <td>{{$order->status}}  <i style=color:green class="fas fa-spinner"></i></td>
        @else
        <td>{{$order->status}}  <i style=color:green class="far fa-check-circle"></i></td>
        @endif
        <td>{{ \Carbon\Carbon::parse($order->created_at)->format('d/m/Y')}}</td>
        
        <td>{{$order->total}} $</td>
        
        <td><a class="btn btn-primary btn-sm" href="/orders/{{$order->id}}">Show details  <i class="fas fa-info"></i></a></td>  
        @if($order->status=="in progress")
        <td><a class="btn btn-success btn-sm" href="/orders/{{$order->id}}/delivered">Delivred  <i class="fas fa-truck-loading"></i></a></td>
        @endif
        
      </tr>
    @empty
<tr>
    <td colspan="3"> no orders found</td>
</tr>
    @endforelse
    </table>
    {{$orders->links("pagination::bootstrap-4")}}
</div>
</div>
</div>
@endsection