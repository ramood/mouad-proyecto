@extends('layouts.app')

@section('content')


	<!--main area-->
	<main id="main" class="main-site">

		<div class="container">
		@if (\Session::has('success'))
    <div class="alert alert-success">
       
          {!! \Session::get('success') !!}
        
    </div>
@endif
		@if (\Session::has('error'))
    <div class="alert alert-danger">
       
           {!! \Session::get('error') !!}</li>
        
    </div>
@endif
			<div class="wrap-breadcrumb">
				<ul>
					<li class="item-link"><a href="#" class="link">home</a></li>
					<li class="item-link"><span>detail</span></li>
				</ul>
			</div>
			<div class="row">

				<div class="col-lg-12 col-md-8 col-sm-8 col-xs-12 main-content-area">
					<div class="wrap-product-detail">
						<div class="detail-media">
							<div class="product-gallery">
							    	<img src="{{ asset('assets/images/products/' . $product->image) }}" alt="product thumbnail" />
							</div>
						</div>
						<div class="detail-info">
							
                            <h2 class="product-name">{{$product->name}}</h2>
                           
                            <div class="wrap-price"><span class="product-price">Price :<b>{{$product->price}}$</b></span></div>
                            <div class="stock-info in-stock">
								<p class="availability">Availability: <b>In Stock</b> </p>
								
                            </div>
							<div class="wrap-butons">
							<form action="/shop/add_to_cart/{{$product->id}}" method="get">
							@csrf
        					@method('PUT')
							<div class="quantity-input ">
								
									<input type="text" id="quantity" name="quantity" value="{{old('quantity', 1)}}" data-max="120" pattern="[0-9]*" >
									<a class="btn btn-increase" onclick="increase()"></a>
									<a class="btn" onclick="reduce()"></a>
								</div>
								<button class="btn add-to-cart col-md-7" >Add to Cart</button>
								
								</form>
							</div>
						</div>
						<div class="advance-info">
							<div class="tab-control normal">
								<a href="#description" class="tab-control-item active">description</a>
							</div>
							<div class="tab-contents">
								<div class="tab-content-item active" id="description">
									<p>{{$product->description}}</p>
								</div>
							
								</div>
							
							</div>
						</div>
					</div>
				</div><!--end main products area-->


		

			</div><!--end row-->

		</div><!--end container-->


    @endsection