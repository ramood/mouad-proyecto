@extends('layouts.app')

@section('content')

<main id="main" class="main-site left-sidebar">

		<div class="container">
		@if (\Session::has('success'))
    <div class="alert alert-success">
       
          {!! \Session::get('success') !!}
        
    </div>
@endif
@if (\Session::has('error'))
    <div class="alert alert-danger">
       
            {!! \Session::get('error') !!}
       
    </div>
@endif

			<div class="wrap-breadcrumb">
				<ul>
					<li class="item-link"><a href="#" class="link">home</a></li>
				
					@if(isset($name))
					<li class="item-link"><span>{{$name}}</span></li>
					
					@else
					<li class="item-link"><span>all</span>
					@endif
				</ul>
			</div>
		
			<div class="row justify-content-center"> 
			@if(!empty($user))
				@if(Auth::user()->isAdmin())
			<a href="/shop/create" class="btn btn-primary " >Add new product</a>
			@endif
			@endif
			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 sitebar">
			@if(!empty($user))
			@if(Auth::user()->isAdmin())
			<a href="/category/create" class="btn btn-primary float-right" >Add new category</a>
			@endif
			@endif
					<div class="widget mercado-widget categories-widget">
					<br>
						<h2 class="widget-title"><a href="/shop" style="color:black">All Categories</a></h2>
						<div class="widget-content">
							<ul class="list-category">
							@foreach($categories as $category)
								<li class="category-item has-child-cate">
									<a href="/shop/sortbyCateg/{{$category->name}}/{{$category->id}}" class="cate-link">{{$category->name}}</a>
									@if(!empty($user))
									@if(Auth::user()->isAdmin())
									<form action="/category/{{$category->id}}" method="post">
											@csrf
											<input type="hidden" name="_method" value="DELETE">
											<input class="btn btn-danger btn-sm"  type="submit" value="X">
											<a href="/category/{{$category->id}}/edit" class="btn "><i class="far fa-edit"></i></a>
											
										</form>
										@endif
										@endif
								</li>
								@endforeach
							</ul>
						</div>
					</div><!-- Categories widget-->


					<div class="widget mercado-widget filter-widget price-filter">
						<h2 class="widget-title">Price</h2>
						
						<form action="/shop/sortbyCateg/{{$name ?? 'all'}}/{{$id ?? '0'}}" method="get">
                   						
						<div class="slidecontainer">
						<input type="range" min="8" max="250" value="8" class="slider" id="min" name="min"><br>
						<p>Min: <span id="demoMin"></span>$</p>
						<input type="range" min="250" max="2000" value="700" class="slider" id="max" name="max"><br>
						<p>Max: <span id="demoMax"></span>$</p>
						</div>
						
						<button style="background:#ff2832;" type="submit" class="btn btn-primary " >Filter<i class="fas fa-funnel-dollar"></i></button>
										</form>
						
						
					</div><!-- Price-->

			


				</div><!--end sitebar-->
				
					<div class="container col-lg-10 col-md-10">
					<div class="row">

						<ul class="product-list grid-products equal-container">

						@forelse($products as $product)
							<li class="col-lg-4 col-md-4 col-sm-6 col-xs-6 ">
								<div class="product product-style-3 equal-elem ">
									<div class="product-thumnail">
										<a href="/shop/{{$product->id}}" title="T-Shirt Raw Hem Organic Boro Constrast Denim">
											<figure><img src="{{ asset('assets/images/products/' . $product->image) }}" /></figure>
											
										</a>
									</div>
									<div class="product-info">
										<a href="#" class="product-name"><span>{{$product->name}}</span></a>
										<div class="wrap-price"><span class="product-price">{{$product->price.'$'}}</span>
										<a href="#" class="btn"></i></a>
										@if(!empty($user))
										@if(Auth::user()->isAdmin())
									<a href="shop/delete/{{$product->id}}" style="color:red;" class="btn">
									<i class="fas fa-trash"></i>
								</a>
											<a href="/shop/{{$product->id}}/edit" class="btn "><i class="far fa-edit"></i></a></div>
											@endif
											@endif
										</form>
										 
										
										<a href="/shop/add_to_cart/{{$product->id}}" class="btn add-to-cart">Add To Cart</a>
									</div>
								</div>
							</li>
						
				
				@empty
<tr>
    <div class="col-md-12"><h1>No results found</h1></div>
</tr>
    @endforelse		
				
				</ul>
				</div>


					
				
				<div class=" col-md-12">
				{{$products->links("pagination::bootstrap-4")}}
				</div>
					
				</div><!--end main products area-->


			</div><!--end row-->

		</div><!--end container-->

	</main>

	<script>
var slider = document.getElementById("min");
var output = document.getElementById("demoMin");
output.innerHTML = slider.value;

slider.oninput = function() {
  output.innerHTML = this.value;
}
var slider2 = document.getElementById("max");
var output2 = document.getElementById("demoMax");
output2.innerHTML = slider2.value;

slider2.oninput = function() {
  output2.innerHTML = this.value;
}
</script>
	@endsection